#!/bin/bash

VAGRANT_NAME=main
VAGRANT_USER=vagrant

VAGRANT_PRIVATE_KEY=.vagrant/machines/${VAGRANT_NAME}/virtualbox/private_key
VAGRANT_PORT=$(vagrant ssh-config ${VAGRANT_NAME} | grep Port | cut -d' ' -f4)

# We can access vagrant machine using this command
ssh -i ${VAGRANT_PRIVATE_KEY} vagrant@localhost -p ${VAGRANT_PORT} hostname

# How to test connectivity with the hosts using ansible
#   -i/--inventory/--inventory-file ${INVENTORY_FILE}
#   -m/--module-name ${MODULE_NAME}
#   -e/--extra-vars ${EXTRA_VARS} as (key=value) or YAML/JSON, if file name prepend with @
#   --private-key ${PRIVATE_KEY_FILE}
ansible localhost -m ping -e ansible_port=${VAGRANT_PORT} -e ansible_user=${VAGRANT_USER} --private-key ${VAGRANT_PRIVATE_KEY}

ansible-playbook -i ./hosts -e ansible_port=${VAGRANT_PORT} -e ansible_user=${VAGRANT_USER} --private-key ${VAGRANT_PRIVATE_KEY} ./nginx-playbook.yml

ansible-playbook -i ./hosts -e ansible_port=${VAGRANT_PORT} -e ansible_user=${VAGRANT_USER} --private-key ${VAGRANT_PRIVATE_KEY} ./connectivity-playbook.yml

# We can run this playbook to run all the playbooks before mentioned
ansible-playbook -i ./hosts -e ansible_port=${VAGRANT_PORT} -e ansible_user=${VAGRANT_USER} --private-key ${VAGRANT_PRIVATE_KEY} ./playbook.yml