#!/bin/bash

VAGRANT_NAME=main

ansible-playbook -i ./hosts.yml --ssh-extra-args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null' --private-key .vagrant/machines/${VAGRANT_NAME}/virtualbox/private_key ./playbook.yml